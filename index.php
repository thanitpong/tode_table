<?php
/**
 * @package TodetableHuay
 * @version 1.0.1
 */

/**
 * Plugin Name: Tode TableHuay
 * Description: ตารางแสดงข้อมูลของหวยทั้งหมด
 * Version: 1.0.1
 */

//Table AllType
function Table_yeekee(){
    $d = '
    <div class="bg-table-header header-tbl-yeekee">
    <div class="bg-table-body body-tbl-yeekee">
    <div class="txt-tblHeader"><label id="txt-Yeekee-1"></label></div>
    <table class="tbl-AllHuay" id="yk-1" name="table">
        <thead>
            <tr>
                <th>หวย ยีกี่</th>
                <th>เวลาปิด</th>
                <th>3 ตัว</th>
                <th>2 ตัว</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    </div>
    </div>
    ';
    return $d;
}
 
add_shortcode('tbl_Yeekee1', 'Table_yeekee');

function Table_Stock(){
    $d = '
    <div class="bg-table-header header-tbl-stock">
    <div class="bg-table-body body-tbl-stock">
    <div class="txt-tblHeader"><label id="txt-stock"></label></div>
    <table class="tbl-AllHuay" id="stock-all" name="table">
        <thead>
            <tr>
                <th>ประเภท</th>
                <th>รอบปิด</th>
                <th>3 ตัวบน</th>
                <th>2 ตัวล่าง</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    </div>
    </div>
    ';
    return $d;
}
 
add_shortcode('tbl_Stock', 'Table_Stock');

function Table_Huay_Goverment_Thai(){
    $d = '
    <div class="bg-table-header header-tbl-gov-thai">
    <div class="bg-table-body body-tbl-gov-thai">
    <div class="txt-tblHeader"><label id="txt-Huaythai"></label></div>
    <table class="tbl-AllHuay" id="Huay-thai" name="table">
        <thead>
            <tr>
                <th>งวดวันที่</th>
                <th>รางวัลที่ 1</th>
                <th>3 ตัวหน้า</th>
                <th>3 ตัวหลัง</th>
                <th>2 ตัว</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    </div>
    </div>
    ';
    return $d;
}
 
add_shortcode('tbl_Huay_Goverment_Thai', 'Table_Huay_Goverment_Thai');

function Table_Huay_Goverment_Hanoy(){
    $d = '
    <div class="bg-table-header header-tbl-gov-hanoy">
    <div class="bg-table-body body-tbl-gov-hanoy">
    <div class="txt-tblHeader"><label id="txt-HuayHanoy"></label></div>
    <table class="tbl-AllHuay" id="Huay-Hanoy" name="table">
        <thead>
            <tr>
                <th>เลขชุด 4 ตัว</th>
                <th>เลข 3 ตัวบน</th>
                <th>เลข 2 ตัวล่าง</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    </div>
    </div>
    ';
    return $d;
}
 
add_shortcode('tbl_Huay_Goverment_Hanoy', 'Table_Huay_Goverment_Hanoy');

function Table_Huay_Goverment_Laos(){
    $d = '
    <div class="bg-table-header header-tbl-gov-laos">
    <div class="bg-table-body body-tbl-gov-laos">
    <div class="txt-tblHeader"><label id="txt-HuayLaos"></label></div>
    <table class="tbl-AllHuay" id="Huay-Laos" name="table">
        <thead>
            <tr>
                <th>4 ໂຕ</th>
                <th>3 ໂຕ</th>
                <th>2 ໂຕ</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    </div>
    </div>
    ';
    return $d;
}
 
add_shortcode('tbl_Huay_Goverment_Laos', 'Table_Huay_Goverment_Laos');

function Table_Huay_Goverment_Malaysia(){
    $d = '
    <div class="bg-table-header header-tbl-gov-malaysia">
    <div class="bg-table-body body-tbl-gov-malaysia">
    <div class="txt-tblHeader"><label id="txt-HuayMalaysia"></label></div>
    <table class="tbl-AllHuay" id="Huay-Malaysia" name="table">
        <thead>
            <tr>
                <th>เลขชุด 4 ตัว</th>
                <th>เลข 3 ตัวบน</th>
                <th>เลข 2 ตัวล่าง</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    </div>
    </div>
    ';
    return $d;
}
 
add_shortcode('tbl_Huay_Goverment_Malaysia', 'Table_Huay_Goverment_Malaysia');

function Table_Huay_Stock_Thai(){
    $d = '
    <div class="bg-table-header header-tbl-stock-thai">
    <div class="bg-table-body body-tbl-stock-thai">
    <div class="txt-tblHeader"><label id="txt-StockThai"></label></div>
    <table class="tbl-AllHuay" id="Huay-StockThai" name="table">
        <thead>
            <tr>
                <th>เลข 3 ตัวบน</th>
                <th>เลข 2 ตัวบน</th>
                <th>เลข ตัวล่าง</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    </div>
    </div>
    ';
    return $d;
}
 
add_shortcode('tbl_Huay_Stock_Thai', 'Table_Huay_Stock_Thai');


//ALL SubHuay table in Country 
function Dashboard_Goverment_Thai(){
    $d = '
    <div class="bg-table-header header-db-gov-thai">
    <div class="bg-table-body body-db-gov-thai">
    <div class="txt-tblHeader-dashboard"><label id="txt-dbGovthai"></label></div>
    <table class="tbl-AllHuay" id="Huay-dbGovthai" name="table">
        <thead>
            <tr>
                <th>หวยรัฐบาลไทย</th>
                <th>งวดที่ <label id="date-dbGovthai"></label></th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    </div>
    </div>
  
    ';
    return $d;
}

add_shortcode('dashboard_Goverment_Thai', 'Dashboard_Goverment_Thai');

function Dashboard_Goverment_Laos(){
    $d = '
    <style>
        td {
            text-align: center;
        }
    </style>
    <div class="bg-table-header header-db-gov-laos">
    <div class="bg-table-body body-db-gov-laos">
    <div class="txt-tblHeader-dashboard"> <label id="txt-dbGovlaos"></label></div>
    <table class="tbl-AllHuay" id="Huay-dbGovlaos" name="table">
        <thead>
            <tr>
                <th>หวยลาว</th>
                <th>งวดที่ <label id="date-dbGovlaos"></label></th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    </div>
    </div>
    ';
    return $d;
}

add_shortcode('dashboard_Goverment_Laos', 'Dashboard_Goverment_Laos');

function Dashboard_Goverment_Malaysia(){
    $d = '
    <style>
        td {
            text-align: center;
        }
    </style>
    <div class="bg-table-header header-db-gov-malaysia">
    <div class="bg-table-body body-db-gov-malaysia">
    <div class="txt-tblHeader-dashboard"><label id="txt-dbGovmalaysia"></label></div>
    <table class="tbl-AllHuay" id="Huay-dbGovmalaysia" name="table">
        <thead>
            <tr>
                <th>หวยมาเลย์</th>
                <th>งวดที่ <label id="date-dbGovmalaysia"></label></th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    </div>
    </div>';
    return $d;
}

add_shortcode('dashboard_Goverment_Malaysia', 'Dashboard_Goverment_Malaysia');

function Dashboard_Goverment_Hanoy_Sp(){
    $d = '
    <div class="bg-table-header header-db-gov-hanoy-sp">
    <div class="bg-table-body body-db-gov-hanoy-sp">
    <div class="txt-tblHeader-dashboard"> <label id="txt-dbGovhanoy-sp"></label></div>
    <table class="tbl-AllHuay" id="Huay-dbGovhanoy-sp" name="table">
        <thead>
            <tr>
                <th>หวยฮานอยพิเศษ</th>
                <th>งวดที่ <label id="date-dbGovhanoy-sp"></label></th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    </div>
    </div>
    ';
    return $d;
}

add_shortcode('dashboard_Goverment_Hanoy_Sp', 'Dashboard_Goverment_Hanoy_Sp');

function Dashboard_Goverment_Hanoy(){
    $d = '
    <div class="bg-table-header header-db-gov-hanoy">
    <div class="bg-table-body body-db-gov-hanoy">
    <div class="txt-tblHeader-dashboard"> <label id="txt-dbGovhanoy"></label></div>
    <table class="tbl-AllHuay" id="Huay-dbGovhanoy" name="table">
        <thead>
            <tr>
                <th>หวยฮานอย</th>
                <th>งวดที่ <label id="date-dbGovhanoy"></label></th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    </div>
    </div>';
    return $d;
}

add_shortcode('dashboard_Goverment_Hanoy', 'Dashboard_Goverment_Hanoy');

function Dashboard_Goverment_Stock_Thai(){
    $d = '
    <div class="bg-table-header header-db-stock-thai">
    <div class="bg-table-body body-db-stock-thai">
    <div class="txt-tblHeader-dashboard"> <label id="txt-dbstockthai"></label></div>
    <table class="tbl-AllHuay" id="Huay-dbstockthai" name="table">
        <thead>
            <tr>
                <th>หวยหุ้นไทย</th>
                <th>งวดที่ <label id="date-dbstockthai"></label></th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    </div>
    </div>';
    return $d;
}

add_shortcode('dashboard_Goverment_Stock_Thai', 'Dashboard_Goverment_Stock_Thai');

//Formula All Table 
function Table_yeekee_2(){
    $d = '
    <div class="bg-table-header header-tbl-yeekee2">
    <div class="bg-table-body body-tbl-yeekee2">
    <div class="txt-tblHeader-Yeekee"><label id="txt-Yeekee-2"></label></div>
    <table class="tbl-AllHuay" id="yk-2" name="table">
        <thead>
            <tr>
                <th>หวย ยีกี่</th>
                <th>เวลาปิด</th>
                <th>3 ตัว</th>
                <th>2 ตัว</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    </div>
    </div';
    return $d;
}
 
add_shortcode('tbl_Yeekee2', 'Table_yeekee_2');

function Table_yeekee_3(){
    $d = '
    <div class="bg-table-header header-tbl-yeekee3">
    <div class="bg-table-body body-tbl-yeekee3">
    <div class="txt-tblHeader-Yeekee"><label id="txt-Yeekee-3"></label></div>
    <table class="tbl-AllHuay" id="yk-3" name="table">
        <thead>
            <tr>
                <th>หวย ยีกี่</th>
                <th>เวลาปิด</th>
                <th>3 ตัว</th>
                <th>2 ตัว</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    </div>
    </div>';
    return $d;
}
 
add_shortcode('tbl_Yeekee3', 'Table_yeekee_3'); 


function tode_table_footer_script(){
    $url = "https://6ab9t9mukb.execute-api.ap-southeast-1.amazonaws.com/v1/result";
    $json = file_get_contents($url);
    $result = json_decode($json, true);
    $result_data = $result['data'];
    echo '
    <script src="https://cdn.jsdelivr.net/npm/lodash@4.17.21/lodash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dayjs/1.10.6/dayjs.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dayjs/1.10.6/locale/th.min.js"></script>
    <script>
    var CheckResult = '.json_encode($result).';
    var result = '.json_encode($result_data).';
    dayjs.locale("th");
    console.log(dayjs().format());
    </script>
    <script>
        //////////////HuayYeekee
        var res_Yeekee_1 = document.getElementById("yk-1");
        if(res_Yeekee_1 != null){
            const res_Yeekee_1 = result.filter(x => x.betGroup == "BET_YEEKEE" && x.setResult == true);
            const res_Yeekee_1_Null = result.filter(x => x.betGroup == "BET_YEEKEE" && x.setResult == false);
            const sorted_Yeekee_1 = _.sortBy(res_Yeekee_1, function(item) {
                var regex = /\d+/g;
                var matches = parseInt(item.betType.match(regex));
                return matches;
            }).reverse();
            const sorted_Yeekee_1_Null = _.sortBy(res_Yeekee_1_Null, function(item) {
                var regex = /\d+/g;
                var matches = parseInt(item.betType.match(regex));
                return matches;
            });
            var empty = [];
            
            var thtml = "";
            var Header_HuayYeeKee_1 = "หวยยี่กี ประจำวันที่ "; 
            for (var i = 0; i < sorted_Yeekee_1.length; i++) {
                if(i == 0 && res_Yeekee_1_Null != empty){
                    thtml += "<tr>";
                    thtml += "<td class='."first-col".'>" + sorted_Yeekee_1_Null[0]["name"] + "</td>";
                    thtml += "<td>" + sorted_Yeekee_1_Null[0]["closeTime"].substring(0, 5); + "</td>";
                    thtml += "<td class='."waiting-col".'>รอผล</td>";
                    thtml += "<td class='."waiting-col".'>รอผล</td>";
                    thtml += "</tr>";
                }
                var datethai = dayjs(sorted_Yeekee_1[i]["resultDate"]).add(543, "year").format("DD MMMM YYYY");
                thtml += "<tr>";
                thtml += "<td class='."first-col".'>" + sorted_Yeekee_1[i]["name"] + "</td>";
                thtml += "<td>" + sorted_Yeekee_1[i]["closeTime"].substring(0, 5); + "</td>";
                thtml += "<td>" + sorted_Yeekee_1[i]["resultBon3"] + "</td>";
                thtml += "<td>" + sorted_Yeekee_1[i]["resultLang2"] + "</td>";
                thtml += "</tr>";
                
            }
            document.getElementById("yk-1").innerHTML += thtml;
            // document.getElementById("yk-2").innerHTML += thtml;
            // document.getElementById("yk-3").innerHTML += thtml;
            document.getElementById("txt-Yeekee-1").innerHTML += Header_HuayYeeKee_1 + datethai;
        }
    </script>    
    <script>    
        //////////////Huay stock
        var res_stock_all = document.getElementById("stock-all");
        if(res_stock_all != null){
            const res_stock_filter = result.filter(x => x.betGroup == "BET_STOCK");
            const res_stock = _.sortBy(res_stock_filter, function(item) {
                var regex = /\d+/g;
                var matches = parseInt(item.closeTime.match(regex));
                if(matches == 1){
                    var Datehms = "1970-01-02 "+item.closeTime;
                }else{
                    var Datehms = "1970-01-01 "+item.closeTime;
                }
                var target = new Date(Datehms);
                return target;
            });
            var thtml = "";
            var Header_HuayStock = "ผลหวยหุ้นวันนี้ "; 
            for (var i = 0; i < res_stock.length; i++) {
                var datethai = dayjs(res_stock[i]["resultDate"]).add(543, "year").format("DD MMMM YYYY");
                var stock_name = res_stock[i]["name"];
                var stock_close = res_stock[i]["closeTime"].substring(0, 5);
                var stock_Bon3 = "รอผล";
                var stock_Lang2 = "รอผล";
                if(res_stock[i]["resultBon"] != ""){
                    stock_Bon3 = res_stock[i]["resultBon3"];
                    stock_Lang2 = res_stock[i]["resultLang2"];
                }
                const words = res_stock[i]["betType"].split("_");
                thtml += "<tr>";
                thtml += "<td class='."first-col".' style='."text-align:left;".'>" + "<img src='.plugins_url( 'assets/image/"+words[2]+".png', __FILE__ ).' width='."18px".' height='."18px".'>" +" "+ stock_name + "</td>";
                thtml += "<td>" + stock_close + "</td>";
                thtml += "<td>" + stock_Bon3 + "</td>";
                thtml += "<td>" + stock_Lang2 + "</td>";
                thtml += "</tr>";

            }
            document.getElementById("stock-all").innerHTML += thtml;
            document.getElementById("txt-stock").innerHTML += Header_HuayStock + datethai;
        }
    </script>      
    <script>  
        /////////////////Huay Thai
        const res_HuayThai = result.filter(x => x.betType == "BET_GOV_THAI");
        var thai_Bon = "รอผล";
        var thai_BonNha1 = "รอผล";
        var thai_BonNha2 = "รอผล";
        var thai_BonLang1 = "รอผล";
        var thai_BonLang2 = "รอผล";
        var thai_Lang2 = "รอผล";

        var res_Huay_thai = document.getElementById("Huay-thai");
        if(res_Huay_thai != null){
            var thtml = "";
            var datethai = "";
            var Header_HuayThai = "ตรวจหวยรัฐบาลงวดล่าสุด "; 
            for (var i = 0; i < res_HuayThai.length; i++) {
                    datethai = dayjs(res_HuayThai[i]["resultDate"]).add(543, "year").format("DD MMMM YYYY");
                    if(res_HuayThai[i]["resultBon"] != ""){
                        thai_Bon = res_HuayThai[i]["resultBon"]
                        thai_BonNha1 = res_HuayThai[i]["resultLangNha31"];
                        thai_BonNha2 = res_HuayThai[i]["resultLangNha32"];
                        thai_BonLang1 = res_HuayThai[i]["resultLang31"];
                        thai_BonLang2 = res_HuayThai[i]["resultLang32"]
                        thai_Lang2 = res_HuayThai[i]["resultLang2"];
                    }
                    thtml += "<tr>";
                    thtml += "<td class='."first-col".'>" + datethai + "</td>";
                    thtml += "<td>" + thai_Bon + "</td>";
                    thtml += "<td>" + thai_BonNha1 +" / "+ thai_BonNha2 + "</td>";
                    thtml += "<td>" + thai_BonLang1 +" / "+ thai_BonLang2 + "</td>";
                    thtml += "<td>" + thai_Lang2 + "</td>";
                    thtml += "</tr>";
            }
            document.getElementById("Huay-thai").innerHTML += thtml;
            document.getElementById("txt-Huaythai").innerHTML += Header_HuayThai + datethai;
        }

        var res_Huay_dbGovthai = document.getElementById("Huay-dbGovthai");
        if(res_Huay_dbGovthai != null){
            var thtml = "";
            var datedbGovthai = "";
            var Header_HuayThai = "ตรวจหวยรัฐบาลงวดล่าสุด "; 
            for (var i = 0; i < res_HuayThai.length; i++) {
                datedbGovthai = dayjs(res_HuayThai[i]["resultDate"]).add(543, "year").format("DD MMM YY");
                if(res_HuayThai[i]["resultBon"] != ""){
                    thai_Bon = res_HuayThai[i]["resultBon"]
                    thai_BonNha1 = res_HuayThai[i]["resultLangNha31"];
                    thai_BonNha2 = res_HuayThai[i]["resultLangNha32"];
                    thai_BonLang1 = res_HuayThai[i]["resultLang31"];
                    thai_BonLang2 = res_HuayThai[i]["resultLang32"]
                    thai_Lang2 = res_HuayThai[i]["resultLang2"];
                }
            }
            var Header_Huay_dbGovthai = "ตรวจหวยรัฐบาลไทยล่าสุด ";
            thtml += "<tr>";
            thtml += "<td class='."first-col".'>รางวัลที่ 1</td>";
            thtml += "<td>" + thai_Bon + "</td>";
            thtml += "</tr>";
            thtml += "<tr>";
            thtml += "<td class='."first-col".'>เลข 3 ตัวหน้า</td>";
            thtml += "<td>" + thai_BonNha1 + " / " + thai_BonNha2 + "</td>";
            thtml += "</tr>";
            thtml += "<tr>";
            thtml += "<td class='."first-col".'>เลข 3 ตัวหลัง</td>";
            thtml += "<td>" + thai_BonLang1 + " / " + thai_BonLang2 + "</td>";
            thtml += "</tr>";
            thtml += "<tr>";
            thtml += "<td class='."first-col".'>เลข 2 ตัว</td>";
            thtml += "<td>" + thai_Lang2 + "</td>";
            thtml += "</tr>";
            document.getElementById("date-dbGovthai").innerHTML += datedbGovthai;
            document.getElementById("Huay-dbGovthai").innerHTML += thtml;
            document.getElementById("txt-dbGovthai").innerHTML += Header_Huay_dbGovthai;
        }
    </script>  
    <script>  
        /////////Huay_Hanoii
        const res_HuayHanoy = result.filter(x => x.betType == "BET_GOV_HANOY");
        var hanoy_Bon = "รอผล";
        var hanoy_Bon4 = "รอผล";
        var hanoy_Bon3 = "รอผล";
        var hanoy_Bon2 = "รอผล";
        var hanoy_Lang2 = "รอผล";

        var res_Huay_Hanoy = document.getElementById("Huay-Hanoy");
        if(res_Huay_Hanoy != null){
            var thtml = "";
            var datehanoy = "";
            var Header_HuayHanoy = "ตรวจหวยฮานอย งวดประจำวันที่ "; 
            for (var i = 0; i < res_HuayHanoy.length; i++) {
                datehanoy = dayjs(res_HuayHanoy[i]["resultDate"]).add(543, "year").format("DD MMMM YYYY");
                if(res_HuayHanoy[i]["resultBon"] != ""){
                    hanoy_Bon = res_HuayHanoy[i]["resultBon"];
                    hanoy_Bon4 = res_HuayHanoy[i]["resultBon4"];
                    hanoy_Bon3 = res_HuayHanoy[i]["resultBon3"];
                    hanoy_Bon2 = res_HuayHanoy[i]["resultBon2"];
                    hanoy_Lang2 = res_HuayHanoy[i]["resultLang2"];
                }
                thtml += "<tr>";
                thtml += "<td>" + hanoy_Bon4 + "</td>";
                thtml += "<td>" + hanoy_Bon3 + "</td>";
                thtml += "<td>" + hanoy_Lang2 + "</td>";
                thtml += "</tr>";
            }
            document.getElementById("Huay-Hanoy").innerHTML += thtml;
            document.getElementById("txt-HuayHanoy").innerHTML += Header_HuayHanoy + datehanoy;
        }

        var res_Huay_dbGovhanoy = document.getElementById("Huay-dbGovhanoy");
        if(res_Huay_dbGovhanoy != null){
            var thtml = "";
            var datedbGovhanoy = "";
            var Header_Huay_dbGovhanoy = "ตรวจหวยฮานอย ล่าสุด ";
            for (var i = 0; i < res_HuayHanoy.length; i++) {
                datedbGovhanoy = dayjs(res_HuayHanoy[i]["resultDate"]).add(543, "year").format("DD MMM YY");
                if(res_HuayHanoy[i]["resultBon"] != ""){
                    hanoy_Bon = res_HuayHanoy[i]["resultBon"];
                    hanoy_Bon4 = res_HuayHanoy[i]["resultBon4"];
                    hanoy_Bon3 = res_HuayHanoy[i]["resultBon3"];
                    hanoy_Bon2 = res_HuayHanoy[i]["resultBon2"];
                    hanoy_Lang2 = res_HuayHanoy[i]["resultLang2"];
                }
            }
            thtml += "<tr>";
            thtml += "<td class='."first-col".'>เลขชุด 4 ตัว</td>";
            thtml += "<td>" + hanoy_Bon4 + "</td>";
            thtml += "</tr>";
            thtml += "<tr>";
            thtml += "<td class='."first-col".'>เลข 3 ตัวบน</td>";
            thtml += "<td>" + hanoy_Bon3 + "</td>";
            thtml += "</tr>";
            thtml += "<tr>";
            thtml += "<td class='."first-col".'>เลข 2 ตัวบน</td>";
            thtml += "<td>" + hanoy_Bon2 + "</td>";
            thtml += "</tr>";
            thtml += "<tr>";
            thtml += "<td class='."first-col".'>เลข 2 ตัวล่าง</td>";
            thtml += "<td>" + hanoy_Lang2 + "</td>";
            thtml += "</tr>";
            
            document.getElementById("date-dbGovhanoy").innerHTML += datedbGovhanoy;
            document.getElementById("Huay-dbGovhanoy").innerHTML += thtml;
            document.getElementById("txt-dbGovhanoy").innerHTML += Header_Huay_dbGovhanoy;
        }
    </script>  
    <script> 

        /////////Huay Loas
        const res_HuayLao = result.filter(x => x.betType == "BET_GOV_LAOS");
        var laos_Bon4 = "รอผล";
        var laos_Bon3 = "รอผล";
        var laos_Lang2 = "รอผล";

        var res_Huay_Laos = document.getElementById("Huay-Laos");
        if(res_Huay_Laos != null){
            var thtml = "";
            var datelaos = "";
            var Header_HuayLaos = "ตรวจหวยลาว งวดประจำวันที่ "; 
            for (var i = 0; i < res_HuayLao.length; i++) {
                datelaos = dayjs(res_HuayLao[i]["resultDate"]).add(543, "year").format("DD MMMM YYYY");
                if(res_HuayLao[i]["resultBon"] != ""){
                    laos_Bon4 = res_HuayLao[i]["resultBon4"];
                    laos_Bon3 = res_HuayLao[i]["resultBon3"];
                    laos_Lang2 = res_HuayLao[i]["resultLang2"];
                }
                thtml += "<tr>";
                thtml += "<td>" + laos_Bon4 + "</td>";
                thtml += "<td>" + laos_Bon3 + "</td>";
                thtml += "<td>" + laos_Lang2 + "</td>";
                thtml += "</tr>";
            }
            document.getElementById("Huay-Laos").innerHTML += thtml;
            document.getElementById("txt-HuayLaos").innerHTML += Header_HuayLaos + datelaos;
        }

        var res_Huay_dbGovlaos = document.getElementById("Huay-dbGovlaos");
        if(res_Huay_dbGovlaos != null){
            var thtml = "";
            var datedbGovlaos = "";
            var Header_Huay_dbGovlaos = "ตรวยหวยลาวล่าสุด ";
            for (var i = 0; i < res_HuayLao.length; i++) {
                datedbGovlaos = dayjs(res_HuayLao[i]["resultDate"]).add(543, "year").format("DD MMM YY");
                if(res_HuayLao[i]["resultBon"] != ""){
                    laos_Bon4 = res_HuayLao[i]["resultBon4"];
                    laos_Bon3 = res_HuayLao[i]["resultBon3"];
                    laos_Lang2 = res_HuayLao[i]["resultLang2"];
                }
            }
            thtml += "<tr>";
            thtml += "<td class='."first-col".'>เลขชุด 4 ตัว</td>";
            thtml += "<td>" + laos_Bon4 + "</td>";
            thtml += "</tr>";
            thtml += "<tr>";
            thtml += "<td class='."first-col".'>เลข 3 ตัวบน</td>";
            thtml += "<td>" + laos_Bon3 + "</td>";
            thtml += "</tr>";
            thtml += "<tr>";
            thtml += "<td class='."first-col".'>เลข 2 ตัวบน</td>";
            thtml += "<td>" + laos_Lang2 + "</td>";
            thtml += "</tr>";
            document.getElementById("date-dbGovlaos").innerHTML += datedbGovlaos;
            document.getElementById("Huay-dbGovlaos").innerHTML += thtml;
            document.getElementById("txt-dbGovlaos").innerHTML += Header_Huay_dbGovlaos;
        }
  </script>  
    <script> 
        ///////////////Malaysia
        const res_HuayMalaysia = result.filter(x => x.betType == "BET_GOV_MALAY");
        var malaysia_Bon = "งดออกรางวัล";
        var malaysia_Bon4 = "งดออกรางวัล";
        var malaysia_Bon3 = "งดออกรางวัล";
        var malaysia_Bon2 = "งดออกรางวัล";
        var malaysia_Lang2 = "งดออกรางวัล";

        var res_Huay_Malaysia = document.getElementById("Huay-Malaysia");
        if(res_Huay_Malaysia != null){
            var thtml = "";
            var datemalaysia = "";
            var Header_HuayMalaysia = "ตรวจหวยมาเลย์ งวดประจำวันที่ "; 
            for (var i = 0; i < res_HuayMalaysia.length; i++) {
                datemalaysia = dayjs(res_HuayMalaysia[i]["resultDate"]).add(543, "year").format("DD MMMM YYYY");
                if(res_HuayMalaysia[i]["resultBon"] != ""){
                    malaysia_Bon = res_HuayMalaysia[i]["resultBon"];
                    malaysia_Bon4 = res_HuayMalaysia[i]["resultBon4"];
                    malaysia_Bon3 = res_HuayMalaysia[i]["resultBon3"];
                    malaysia_Bon2 = res_HuayMalaysia[i]["resultBon2"];
                    malaysia_Lang2 = res_HuayMalaysia[i]["resultLang2"];
                }
                thtml += "<tr>";
                thtml += "<td>" + malaysia_Bon4 + "</td>";
                thtml += "<td>" + malaysia_Bon3 + "</td>";
                thtml += "<td>" + malaysia_Lang2 + "</td>";
                thtml += "</tr>";
            }
            document.getElementById("Huay-Malaysia").innerHTML += thtml;
            document.getElementById("txt-HuayMalaysia").innerHTML += Header_HuayMalaysia + datemalaysia;
        }

        var res_Huay_dbGovmalaysia = document.getElementById("Huay-dbGovmalaysia");
        if(res_Huay_dbGovmalaysia != null){
            var thtml = "";
            var datedbGovmalaysia = "";
            var Header_Huay_dbGovmalaysia = "ตรวจหวยมาเลย์ ";
            for (var i = 0; i < res_HuayMalaysia.length; i++) {
                datedbGovmalaysia = dayjs(res_HuayMalaysia[i]["resultDate"]).add(543, "year").format("DD MMM YY");
                if(res_HuayMalaysia[i]["resultBon"] != ""){
                    malaysia_Bon = res_HuayMalaysia[i]["resultBon"];
                    malaysia_Bon4 = res_HuayMalaysia[i]["resultBon4"];
                    malaysia_Bon3 = res_HuayMalaysia[i]["resultBon3"];
                    malaysia_Bon2 = res_HuayMalaysia[i]["resultBon2"];
                    malaysia_Lang2 = res_HuayMalaysia[i]["resultLang2"];
                }
            }
            thtml += "<tr>";
            thtml += "<td class='."first-col".'>เลขชุด 4 ตัว</td>";
            thtml += "<td>" + malaysia_Bon + "</td>";
            thtml += "</tr>";
            thtml += "<tr>";
            thtml += "<td class='."first-col".'>เลข 3 ตัวบน</td>";
            thtml += "<td>" + malaysia_Bon3 + "</td>";
            thtml += "</tr>";
            thtml += "<tr>";
            thtml += "<td class='."first-col".'>เลข 2 ตัวบน</td>";
            thtml += "<td>" + malaysia_Bon2 + "</td>";
            thtml += "</tr>";
            thtml += "<tr>";
            thtml += "<td class='."first-col".'>เลข 2 ตัวล่าง</td>";
            thtml += "<td>" + malaysia_Lang2 + "</td>";
            thtml += "</tr>";
            
            document.getElementById("date-dbGovmalaysia").innerHTML += datedbGovmalaysia;
            document.getElementById("Huay-dbGovmalaysia").innerHTML += thtml;
            document.getElementById("txt-dbGovmalaysia").innerHTML += Header_Huay_dbGovmalaysia;
        }
  </script>  
    <script> 
        /////stock thai 
        const res_HuayStockThai = result.filter(x => x.betType == "BET_STOCK_THAI_AFTERNOON");
        var stockthai_Bon3 = "รอผล";
        var stockthai_Bon2 = "รอผล";
        var stockthai_Lang2 = "รอผล";
        
        var res_Huay_StockThai = document.getElementById("Huay-StockThai");
        if(res_Huay_StockThai != null){
            var thtml = "";
            var datethai = "";
            var Header_HuayStockThai = "ผลหวยหุ้น งวดวันที่ "; 
            for (var i = 0; i < res_HuayStockThai.length; i++) {
                datethai = dayjs(res_HuayStockThai[i]["resultDate"]).add(543, "year").format("DD MMMM YYYY");
                if(res_HuayStockThai[i]["resultBon"] != ""){
                    stockthai_Bon3 = res_HuayStockThai[i]["resultBon3"];
                    stockthai_Bon2 = res_HuayStockThai[i]["resultBon2"];
                    stockthai_Lang2 = res_HuayStockThai[i]["resultLang2"];
                }
                thtml += "<tr>";
                thtml += "<td>" + stockthai_Bon3 + "</td>";
                thtml += "<td>" + stockthai_Bon2 + "</td>";
                thtml += "<td>" + stockthai_Lang2 + "</td>";
                thtml += "</tr>";
            }
            document.getElementById("Huay-StockThai").innerHTML += thtml;
            document.getElementById("txt-StockThai").innerHTML += Header_HuayStockThai + datethai;
        }

        var res_Huay_dbstockthai = document.getElementById("Huay-dbstockthai");
        if(res_Huay_dbstockthai != null){
            var thtml = "";
            var datedbstockthai = "";
            var Header_Huay_dbstockthai = "ตรวจหวยหุ้น ล่าสุด ";
            for (var i = 0; i < res_HuayStockThai.length; i++) {
                datedbstockthai = dayjs(res_HuayStockThai[i]["resultDate"]).add(543, "year").format("DD MMM YY");
                if(res_HuayStockThai[i]["resultBon"] != ""){
                    stockthai_Bon3 = res_HuayStockThai[i]["resultBon3"];
                    stockthai_Bon2 = res_HuayStockThai[i]["resultBon2"];
                    stockthai_Lang2 = res_HuayStockThai[i]["resultLang2"];
                }
            }
            thtml += "<tr>";
            thtml += "<td class='."first-col".'>เลข 3 ตัวบน</td>";
            thtml += "<td>" + stockthai_Bon3 + "</td>";
            thtml += "</tr>";
            thtml += "<tr>";
            thtml += "<td class='."first-col".'>เลข 2 ตัวบน</td>";
            thtml += "<td>" + stockthai_Bon2 + "</td>";
            thtml += "</tr>";
            thtml += "<tr>";
            thtml += "<td class='."first-col".'>เลข 2 ตัวล่าง</td>";
            thtml += "<td>" + stockthai_Lang2 + "</td>";
            thtml += "</tr>";
            
            document.getElementById("date-dbstockthai").innerHTML += datedbstockthai;
            document.getElementById("Huay-dbstockthai").innerHTML += thtml;
            document.getElementById("txt-dbstockthai").innerHTML += Header_Huay_dbstockthai;
        }
  </script>  
    <script> 

        //// hanoi sp 
        var res_Huay_dbGovhanoy_sp = document.getElementById("Huay-dbGovhanoy-sp");
        if(res_Huay_dbGovhanoy_sp != null){
            const res_Huay_dbGovhanoy_sp = result.filter(x => x.betType == "BET_GOV_HANOY_SP");
            var hanoy_Bon_sp = "รอผล";
            var hanoy_Bon4_sp = "รอผล";
            var hanoy_Bon3_sp = "รอผล";
            var hanoy_Bon2_sp = "รอผล";
            var hanoy_Lang2_sp= "รอผล";
            var datehanoysp = "";
            for (var i = 0; i < res_Huay_dbGovhanoy_sp.length; i++) {
                datehanoysp = dayjs(res_Huay_dbGovhanoy_sp[i]["resultDate"]).add(543, "year").format("DD MMM YY");
                if(res_Huay_dbGovhanoy_sp[i]["resultBon"] != ""){
                    hanoy_Bon_sp = res_Huay_dbGovhanoy_sp[i]["resultBon"];
                    hanoy_Bon4_sp = res_Huay_dbGovhanoy_sp[i]["resultBon4"];
                    hanoy_Bon3_sp = res_Huay_dbGovhanoy_sp[i]["resultBon3"];
                    hanoy_Bon2_sp = res_Huay_dbGovhanoy_sp[i]["resultBon2"];
                    hanoy_Lang2_sp = res_Huay_dbGovhanoy_sp[i]["resultLang2"];
                }
            }
            var thtml = "";
            var Header_Huay_dbGovhanoy_sp = "ตรวจหวยฮานอยพิเศษ ล่าสุด ";
            thtml += "<tr>";
            thtml += "<td class='."first-col".'>เลขชุด 4 ตัว</td>";
            thtml += "<td>" + hanoy_Bon4_sp + "</td>";
            thtml += "</tr>";
            thtml += "<tr>";
            thtml += "<td class='."first-col".'>เลข 3 ตัวบน</td>";
            thtml += "<td>" + hanoy_Bon3_sp + "</td>";
            thtml += "</tr>";
            thtml += "<tr>";
            thtml += "<td class='."first-col".'>เลข 2 ตัวบน</td>";
            thtml += "<td>" + hanoy_Bon2_sp + "</td>";
            thtml += "</tr>";
            thtml += "<tr>";
            thtml += "<td class='."first-col".'>เลข 2 ตัวล่าง</td>";
            thtml += "<td>" + hanoy_Lang2_sp + "</td>";
            thtml += "</tr>";
                
            document.getElementById("date-dbGovhanoy-sp").innerHTML += datehanoysp;
            document.getElementById("Huay-dbGovhanoy-sp").innerHTML += thtml;
            document.getElementById("txt-dbGovhanoy-sp").innerHTML += Header_Huay_dbGovhanoy_sp;
        }
    </script>';

}

add_action( 'wp_footer', 'tode_table_footer_script' );

?>


